#
# Cookbook:: apacheProject
# Recipe:: default
#
# Copyright:: 2017, The Authors, All Rights Reserved.

# Install appropriate packages and enable + start the service
if node['platform_version'] == '14.04'
  apt_update
  package %w(apache2 php5 libapache2-mod-php5)
  apachesvc = 'apache2'
elsif node['platform_version'] == '16.04'
  apt_update
  package %w(apache2 php7.0 libapache2-mod-php7.0)
  apachesvc = 'apache2'
elsif node['platform_version'] == '18.04'
  apt_update
  package %w(apache2 php7.2 libapache2-mod-php7.2)
  apachesvc = 'apache2'
elsif node['platform'] == 'centos'
  package %w(httpd php)
  apachesvc = 'httpd'
end

service apachesvc do
  supports [:enable, :start, :stop, :reload, :restart]
  action [:enable, :start]
end

# Create and install a php info file
file '/var/www/html/info.php' do
  content '<?php phpinfo() ?>'
  mode '0755'
  if node['platform'] == 'ubuntu'
    owner 'www-data'
    group 'www-data'
  else
    owner 'apache'
    group 'apache'
  end
end
