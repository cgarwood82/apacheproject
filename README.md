# apacheProject

My submission for the chef challenge. Relatively basic:

* Install Apache and php on CentOS 6 and 7 as well as all supported LTS Ubuntu Releases
* Configure, enable, and start the Apache service
* Create php info page

Further, unit tests and functional tests have also been developed. 

# How to use

You need the chefdk, vagrant, and virtualbox installed for this to work. Get with the program.

## To converge
```
kitchen converge
```

## To verify
```
kitchen verify
```

## To run unit tests
```
chef exec rspec
```

## To run all of the tests
```
kitchen test
```
