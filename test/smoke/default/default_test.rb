# Inspec test for recipe apacheProject::default

# The Inspec reference, with examples and extensive documentation, can be
# found at http://inspec.io/docs/reference/resources/

# Ensure port is is open
describe port(80) do
  it { should be_listening }
  its('protocols') { should include 'tcp' }
end

# Ensure service is installed, running, and enabled
if os[:family] == 'debian'
  describe service('apache2') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
else
  describe service('httpd') do
    it { should be_installed }
    it { should be_enabled }
    it { should be_running }
  end
end

# Ensure the file is being server properly
describe command('curl localhost/info.php') do
  its('exit_status') { should eq 0 }
end

# Ensure proper file stuff and things
if os[:name] == 'ubuntu'
  describe file('/var/www/html/info.php') do
    it { should exist }
    its('mode') { should cmp '0755' }
    its('owner') { should eq 'www-data' }
    its('group') { should eq 'www-data' }
  end
elsif describe file('/var/www/html/info.php') do
  it { should exist }
  its('mode') { should cmp '0755' }
  its('owner') { should eq 'apache' }
  its('group') { should eq 'apache' }
end
end
