name 'apacheProject'
maintainer 'Clifford Garwood'
maintainer_email 'cliff@cigii.com'
license 'All Rights Reserved'
description 'Installs/Configures apache'
long_description 'Installs/Configures apache for support Ubuntu LTS releases as
 well as CentOS 6 and 7'
version '0.1.2'
chef_version '>= 14.6' if respond_to?(:chef_version)
supports 'ubuntu', '>= 16.04'
supports 'centos'
license 'GPL-3.0'
issues_url 'https://bitbucket.org/cgarwood82/apacheproject'
source_url 'https://bitbucket.org/cgarwood82/apacheproject'
